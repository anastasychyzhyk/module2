using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Runtime.CompilerServices;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Program a = new Program();
            Console.Write(a.GetCongratulation(1));
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return companiesNumber * tax * companyRevenue / 100;
        }

        public string GetCongratulation(int input)
        {
            string msg;
            if ((input >= 18) && (input % 2 == 0))
                msg = "Поздравляю с совершеннолетием!";
            else if ((input < 18) && (input > 12) && (input % 2 != 0))
                msg = "Поздравляю с переходом в старшую школу!";
            else
                msg = "Поздравляю с " + input.ToString() + "-летием!";
            return msg;
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            double first_double, second_double;
            IFormatProvider formatter1 = new NumberFormatInfo { NumberDecimalSeparator = "." };
            IFormatProvider formatter2 = new NumberFormatInfo { NumberDecimalSeparator = "," };
            if (!double.TryParse(first, NumberStyles.Float, formatter1, out first_double) && !double.TryParse(first, NumberStyles.Float, formatter2, out first_double))
                throw new ArgumentException("Input data error", "first");
            if (!double.TryParse(second, NumberStyles.Float, formatter1, out second_double) && !double.TryParse(second, NumberStyles.Float, formatter2, out second_double))
                throw new ArgumentException("Input data error", "second");
            double res = first_double * second_double;
            return res;
        }

        public double GetFigureValues(FigureType figureType, FigureParameter parameterToCompute, Dimensions dimensions)
        {
            double res = 0;
            switch (figureType)
            {
                case FigureType.Triangle:
                    if (parameterToCompute == FigureParameter.Perimeter)
                        res = dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide;
                    else if (dimensions.Height != 0)
                        res = dimensions.FirstSide * dimensions.Height / 2.0;
                    else
                    {
                        double p = (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide) / 2.0;
                        res = Math.Sqrt(p * (p - dimensions.FirstSide) * (p - dimensions.SecondSide) * (p - dimensions.ThirdSide));
                    }
                    break;
                case FigureType.Rectangle:
                    if (parameterToCompute == FigureParameter.Perimeter)
                        res = 2.0 * (dimensions.FirstSide + dimensions.SecondSide);
                    else
                        res = dimensions.FirstSide * dimensions.SecondSide;
                    break;
                case FigureType.Circle:
                    if ((parameterToCompute == FigureParameter.Perimeter) && (dimensions.Diameter != 0))
                        res = Math.PI * dimensions.Diameter;
                    else if ((parameterToCompute == FigureParameter.Square) && (dimensions.Diameter != 0))
                        res = Math.PI * dimensions.Diameter * dimensions.Diameter / 4.00;
                    else if (parameterToCompute == FigureParameter.Perimeter)
                        res = 2.0 * Math.PI * dimensions.Radius;
                    else
                        res = Math.PI * Math.Pow(dimensions.Radius, 2.0);
                    res = Math.Round(res);
                    break;
                default: break;
            }
            return Math.Truncate(res);
        }
    }
}
